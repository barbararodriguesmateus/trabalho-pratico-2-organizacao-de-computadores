import java.util.Random;
import java.util.Scanner;

import javax.swing.text.AbstractDocument.Content;

//o * quer dizer que importamos todas as bibliotecas relativas à java.util.
//a biblioteca que estamos importando aqui é a Random, responsavel pelo tipo de variavel "Random"
public class TpORG {

    Scanner in = new Scanner(System.in);
    Random r = new Random();
    bloco[] memoryData = new bloco[SIZERAM]; // criamos um vetor memoryData, com 1000 posições (RAM)
    int[][] memoriaInstrucoes;
    static int SIZERAM = 1000;
    static int SIZECACHE1 = 32, SIZECACHE2 = 64, SIZECACHE3 = 128;
    static int amountInstructions = 100;
    static int C1 = 10, C2 = 100, C3 = 1000, CRAM= 10000;
    int operador; // variável global ultilizada para operações
    int Null = 0;
    int custo = 0;
    int time = 0;
    

    
    Cache1 cache1 = new Cache1();
    Cache2 cache2 = new Cache2();
    Cache3 cache3 = new Cache3();
  

    public static void main(String[] args) {
        new TpORG();
    }

    public TpORG() {

        MontaRameCaches();

        waitForInstructions();
    }

    public static class Cache1
    {
        bloco[] bloco = new bloco[SIZECACHE1];
        int counter;
        int hit;
        int miss;

    }
    public static class Cache2
    {
        bloco[] bloco = new bloco[SIZECACHE2];
        int counter;
        int hit;
        int miss;


    }
    public static class Cache3
    {
        bloco[] bloco = new bloco[SIZECACHE3];
        int counter;
        int hit;
        int miss;
 
    }

    void MontaRameCaches() {
        int accumulate = 0;
        // bilioteca que importamos.
        for (int i = 0; i < SIZERAM; i++) {
            memoryData[i] = new bloco();
            memoryData[i].tag = i;
            memoryData[i].hit = 0;
            memoryData[i].atualizado = false;
            memoryData[i].T = 0;
            for (int j = 0; j < 4; j++) {
                memoryData[i].Palavra[j] = new palavra();
                memoryData[i].Palavra[j].endereco = accumulate;
                accumulate++;
            }
            // vamos ter valores de 0 a 99 em cada uma das posições do nosso vetor
        }

        cache1 = new Cache1();
        cache2 = new Cache2();
        cache3 = new Cache3();
        for (int i = 0; i < SIZECACHE3; i++)
        {
            if(i < SIZECACHE1)
            {
                cache1.bloco[i] = new bloco();
                cache1.hit = 0;
                cache1.miss = 0;
                cache1.counter = 0;
              
                for (int j = 0; j < 4; j++)
                {
                    cache1.bloco[i].Palavra[j] = new palavra(); 
                }
                
            }

            if(i < SIZECACHE2)
            {
                cache2.bloco[i] = new bloco();
                cache2.hit = 0;
                cache2.miss = 0;
                cache2.counter = 0;
                
                for (int j = 0; j < 4; j++)
                {
                    cache2.bloco[i].Palavra[j] = new palavra(); 
                }
                
            }

            if(i < SIZECACHE3)
            {
                cache3.bloco[i] = new bloco();
                cache2.hit = 0;
                cache3.miss = 0;
                cache2.counter = 0;

                for (int j = 0; j < 4; j++)
                {
                    cache3.bloco[i].Palavra[j] = new palavra(); 
                }
                
            }
        }

    }


    int cache1Status(Cache1 cache1){
        //1 for full
        //2 for empty
        //3 for neither empty nor full
        if (cache1.counter == SIZECACHE1)
        {
            return 1;
        }
        else if (cache1.counter == 0)
        {
            return 2;
        }else
        {
            return 3;
        }
    }

    int cache2Status(Cache2 cache2){
        //1 for full
        //2 for empty
        //3 for neither empty nor full
        if (cache2.counter == SIZECACHE2)
        {
            return 1;
        }
        else if (cache2.counter == 0)
        {
            return 2;
        }else
        {
            return 3;
        }
    }

    int cache3Status(Cache3 cache3){
        //1 for full
        //2 for empty
        //3 for neither empty nor full
        if (cache3.counter == SIZECACHE3)
        {
            return 1;
        }
        else if (cache3.counter == 0)
        {
            return 2;
        }else
        {
            return 3;
        }
    }

    bloco popFromCache1 (Cache1 cache1, int index)
    {
        bloco toBeReturned = cache1.bloco[index];
        for (int i = index+1; i < cache1.counter; i++)
        {
            cache1.bloco[i-1] = cache1.bloco[i];
        }
        cache1.counter--;
        return toBeReturned;
    }


    bloco popFromCache2 (Cache2 cache2, int index)
    {
        bloco toBeReturned = cache2.bloco[index];
        for (int i = index+1; i < cache2.counter; i++)
        {
            cache2.bloco[i-1] = cache2.bloco[i];
        }
        cache2.counter--;
        return toBeReturned;
    }

    bloco popFromCache3 (Cache3 cache3, int index)
    {
        bloco toBeReturned = cache3.bloco[index];
        for (int i = index+1; i < cache3.counter; i++)
        {
            cache3.bloco[i-1] = cache3.bloco[i];
        }
        cache3.counter--;
        return toBeReturned;
    }

    void insertIntoCache1 (Cache1 cache1, bloco item)
    {
        cache1.bloco[cache1.counter++] = item;

    }

    void insertIntoCache2 (Cache2 cache2, bloco item)
    {
        cache2.bloco[cache2.counter++] = item;
  
    }

    void insertIntoCache3 (Cache3 cache3, bloco item)
    {
        cache3.bloco[cache3.counter++] = item;
 
    }

    void DEBUGGER(bloco Bloco)
    {
        System.out.println("Tag do bloco: " + Bloco.tag);

        for(int i = 0; i < 4; i++)
        {
            System.out.println("Tag da palavra " + i + " : " + Bloco.Palavra[i].endereco);
            System.out.println("Conteudo da palavra " + i + " : " + Bloco.Palavra[i].valor);
        }
    }
	
	public class Address {
		int[] end = new int[2];
   
	}

	public class main {
		int opCode;
		Address[] content = new Address[4];
    
	}
    
    void instructionVector (int[] instruction)
    {
        instruction[amountInstructions-1] = -1;
        for (int i = 0; i < amountInstructions -1; i++)
        {   
            instruction[i] = r.nextInt(9) + 1;
        }
    }

    void waitForInstructions() {

        System.out.println("Digite 1 para o modo ALEATÓRIO e 2 para o modo ENTRADA:");
        int io = in.nextInt();

        main[] memoriaInstrucoes = new main[100]; // cria uma matrix 100x5

        switch (io){
            case 1:
            {
                int i = 0, op = 0, auxiliar = 0;

                main mainInstruction = new main(); // registrador

                int[] vector = new int[amountInstructions];
                instructionVector(vector);

                do {
                    custo = 0;
                    cache1.hit = 0;
                    cache1.miss = 0;
                    cache2.hit = 0;
                    cache2.miss = 0;
                    cache3.hit = 0;
                    cache3.miss = 0;
                    
                    for (int j = 0; j < 4; j++){
                        mainInstruction.content[j] = new Address();
                    }
                    
                    int operation = vector[auxiliar];
                    auxiliar++;
                    int endPalavra;
                    int endBloco;
                
                    endPalavra = r.nextInt(SIZERAM * 4);
                    endBloco = endPalavra / 4;
        
                    mainInstruction.opCode = operation;
                    mainInstruction.content[0].end[0] = endBloco;
                    mainInstruction.content[0].end[1] = endPalavra;
        
                    endPalavra = r.nextInt(SIZERAM * 4);
                    endBloco = endPalavra / 4 ;
                    mainInstruction.content[1].end[0] = endBloco;
                    mainInstruction.content[1].end[1] = endPalavra;
        
                    endPalavra = r.nextInt(SIZERAM * 4);
                    endBloco = endPalavra / 4;
                    mainInstruction.content[2].end[0] = endBloco;
                    mainInstruction.content[2].end[1] = endPalavra;
        
                    endPalavra = r.nextInt(SIZERAM * 4);
                    endBloco = endPalavra / 4;
                    mainInstruction.content[3].end[0] = endBloco;
                    mainInstruction.content[3].end[1] = endPalavra;

                    switch (operation) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 7:
                        case 8:{
                            int num1 = r.nextInt(101);
                            mainInstruction.opCode = 2;
                            mainCPU(mainInstruction, num1, 1);
                            int num2 = r.nextInt(101);
                            mainInstruction.opCode = 2;
                            mainCPU(mainInstruction, num2, 2);
    
                            CPU(mainInstruction, operation);
                            memoriaInstrucoes[i] = mainInstruction;
                            i++;
                            break;
                        }
                        case 6: {
                            int num1 = r.nextInt(201);
                            mainInstruction.opCode = 2;
                            mainCPU(mainInstruction, num1, 1);// salvando
                            CPU(mainInstruction, operation);
                            memoriaInstrucoes[i] = mainInstruction;
                            i++;
                            break;
                        }
                        case 5:
                        {
                            int num1 = r.nextInt(50);
                            mainInstruction.opCode = 2;
                            mainCPU(mainInstruction, num1, 1);
                            int num2 = r.nextInt(5);
                            mainInstruction.opCode = 2;
                            mainCPU(mainInstruction, num2, 2);
    
                            CPU(mainInstruction, operation);
                            memoriaInstrucoes[i] = mainInstruction;
                            i++;
                            break;                         
                        }
                        case 9: {
                            int num1 = r.nextInt(201);
                            mainInstruction.opCode = 2;
                            mainCPU(mainInstruction, num1, 1);// salvando
                            int num2 = r.nextInt(4);
                            mainInstruction.opCode = 2;
                            mainCPU(mainInstruction, num2, 2);
                            CPU(mainInstruction, operation);
                            memoriaInstrucoes[i] = mainInstruction;
                            i++;
                            break;
                        }
                        case -1: {
                            CPU(mainInstruction, operation);
                            break;
                        }
    
                    }
        
                    
                } while (op != -1);

                break;
            }
            case 2:
            {
                int op = 0;
                do {
                    
                    int i = 0;
                    System.out.printf(" ===============" + "\n MAQUINA VIRTUAL " + "\n ===============" + "\n "
                            + "Digite uma instrucao : " + "\n 1 - SOMA " + "\n 2 - SUBTRACAO " + "\n 3 - MULTIPLICACAO "
                            + "\n 4 - DIVISAO " + "\n 5 - EXPONENCIAL " + "\n 6 - RAIZ QUADRADA " + "\n 7 - HIPOTENUSA "
                            + "\n 8 - AREA DE TRIANGULO " + "\n 9 - RAIZ QUALQUER " + "\n Ou -1 para sair... >>");
                          
                    main mainInstruction = new main(); // registrador
                    custo = 0;
                    cache1.hit = 0;
                    cache1.miss = 0;
                    cache2.hit = 0;
                    cache2.miss = 0;
                    cache3.hit = 0;
                    cache3.miss = 0;
                   
                    for (int j = 0; j < 4; j++){
                        mainInstruction.content[j] = new Address();
                    }
                    
                    int operation = in.nextInt();
                    int endPalavra;
                    int endBloco;
                
                    endPalavra = r.nextInt(SIZERAM * 4);
                    endBloco = endPalavra / 4;
        
                    mainInstruction.opCode = operation;
                    mainInstruction.content[0].end[0] = endBloco;
                    mainInstruction.content[0].end[1] = endPalavra;
        
                    endPalavra = r.nextInt(SIZERAM * 4);
                    endBloco = endPalavra / 4 ;
                    mainInstruction.content[1].end[0] = endBloco;
                    mainInstruction.content[1].end[1] = endPalavra;
        
                    endPalavra = r.nextInt(SIZERAM * 4);
                    endBloco = endPalavra / 4;
                    mainInstruction.content[2].end[0] = endBloco;
                    mainInstruction.content[2].end[1] = endPalavra;
        
                    endPalavra = r.nextInt(SIZERAM * 4);
                    endBloco = endPalavra / 4;
                    mainInstruction.content[3].end[0] = endBloco;
                    mainInstruction.content[3].end[1] = endPalavra;
                  
                    if (operation > 9 || operation < -1) {
                        System.out.println("\n Número inválido, o programa será encerrado.");
                        break;
                    } else {
                        switch (operation) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5: {
                                System.out.println("\n Digite o primeiro numero: ");
                                int num1 = in.nextInt();
                                mainInstruction.opCode = 2;
                                mainCPU(mainInstruction, num1, 1);
                                System.out.println("\n Digite o segundo numero: ");
                                int num2 = in.nextInt();
                                mainInstruction.opCode = 2;
                                mainCPU(mainInstruction, num2, 2);
        
                                CPU(mainInstruction, operation);
                                memoriaInstrucoes[i] = mainInstruction;
                                i++;
                                break;
                            }
                            case 6: {
                                System.out.println("\n Digite o numero que deseja saber a raiz quadrada: ");
                                int num1 = in.nextInt();
                                mainInstruction.opCode = 2;
                                mainCPU(mainInstruction, num1, 1);// salvando
                                CPU(mainInstruction, operation);
                                memoriaInstrucoes[i] = mainInstruction;
                                i++;
                                break;
                            }
                            case 7: {
                                System.out.println("\n Digite o valor do cateto adjacente: ");
                                int num1 = in.nextInt();
                                mainInstruction.opCode = 2;
                                mainCPU(mainInstruction, num1, 1);// salvando
                                System.out.println("\n Digite o valor do cateto oposto: ");
                                int num2 = in.nextInt();
                                mainInstruction.opCode = 2;
                                mainCPU(mainInstruction, num2, 2);
                                CPU(mainInstruction, operation);
                                memoriaInstrucoes[i] = mainInstruction;
                                i++;
                                break;
                            }
                            case 8: {
                                System.out.println("\n Digite o valor da altura do triangulo: ");
                                int num1 = in.nextInt();
                                mainInstruction.opCode = 2;
                                mainCPU(mainInstruction, num1, 1);// salvando
                                System.out.println("\n Digite o valor da base do triangulo: ");
                                int num2 = in.nextInt();
                                mainInstruction.opCode = 2;
                                mainCPU(mainInstruction, num2, 2);
                                CPU(mainInstruction, operation);
                                memoriaInstrucoes[i] = mainInstruction;
                                i++;
                                break;
                            }
                            case 9: {
                                System.out.println("\n Digite o valor do radicando da raiz a ser calculada: ");
                                int num1 = in.nextInt();
                                mainInstruction.opCode = 2;
                                mainCPU(mainInstruction, num1, 1);// salvando
                                System.out.println("\n Digite o valor indice da raiz a ser calculada: ");
                                int num2 = in.nextInt();
                                mainInstruction.opCode = 2;
                                mainCPU(mainInstruction, num2, 2);
                                CPU(mainInstruction, operation);
                                memoriaInstrucoes[i] = mainInstruction;
                                i++;
                                break;
                            }
                            case -1: {
                                CPU(mainInstruction, operation);
                                break;
                            }
        
                        }
        
                    }
                } while (op != -1);
                break;
            }
        }


    }

    public class bloco {
        palavra[] Palavra = new palavra[4];
        int tag;
        boolean atualizado;
        int hit;
        int T;

    }

    public class palavra {
        int endereco;
        int valor;
    }

    int mainCPU(main mainInstruction, int num, int where) {
        int opcode = mainInstruction.opCode;
        switch (opcode) {
            case 0: {
                return Sum(mainInstruction);
            }

            case 1: {
                return sub(mainInstruction);
            }

            case 2: {
                int blocoID,palavraID;

                blocoID = mainInstruction.content[where-1].end[0];
                palavraID = mainInstruction.content[where-1].end[1];
                storeInMem(opcode, blocoID, palavraID, num, memoryData, cache1, cache2, cache3);
                break;
            }
            case 3: {
                int blocoID,palavraID;

                blocoID = mainInstruction.content[where-1].end[0];
                palavraID = mainInstruction.content[where-1].end[1];

                return takeFromMem(opcode, blocoID, palavraID, memoryData, cache1, cache2, cache3);
            }
            case -1: {
                saveAfterHalt(cache1, cache2, cache3, memoryData);
                System.exit(0);
                break;
            }
        }
        return 0;
    }

    void printResult(main instruction, int operation) {
        instruction.opCode = 3;
        int num1 = mainCPU(instruction, Null, 1);
        int num2 = mainCPU(instruction, Null, 2);
        int num3 = mainCPU(instruction, Null, 3);

        switch (operation) {
            case 1: {
                System.out.println("Somando " + num1 + " com " + num2 + " e gerando " + num3);
                break;
            }
            case 2: {
                System.out.println("Subtraindo " + num1 + " por " + num2 + " e gerando " + num3);
                break;
            }
            case 3: {
                System.out.println("Multiplicando " + num1 + " por " + num2 + " e gerando " + num3);
                break;
            }
            case 4: {
                int num4 = mainCPU(instruction, Null, 4);
                if (num3 == -1 && num4 == -1) {
                    System.out.println("NAO EH POSSIVEL FAZER DIVISAO COM DIVISOR 0");
                } else {
                    System.out.println(
                            "Dividindo " + num1 + " por " + num2 + " e gerando " + num3 + " com resto " + num4);
                }

                break;
            }
            case 5: {
                System.out.println("Elevando " + num1 + " por " + num2 + " e gerando " + num3);
                break;
            }
            case 6: {
                System.out.println("Raiz quadrada de " + num1 + " = " + num3);
                break;
            }
            case 7: {
                System.out.println("Hipotenusa de triangulo de catetos " + num1 + " e " + num2 + " = " + num3);
                break;
            }
            case 8: {
                System.out.println("Area do triangulo de altura " + num1 + " e base " + num2 + " = " + num3);
                break;
            }
            case 9: {
                if (num3 == -9876) {
                    System.out.println("Nao existe raiz de indice " + num2);
                } else if (num3 == -6789) {
                    System.out.println(
                            "Nao existe um resultado inteiro para a raiz de indice " + num2 + " de radicando " + num1);
                } else if (num3 == -9999) {
                    System.out.println("Nao existe resultado para uma raiz de indice par e radicando impar");
                } else {
                    System.out.println("A raiz de indice " + num2 + " e de radicando " + num1 + " eh " + num3);
                }

                break;
            }

        }

        System.out.println("Hit C1: "+ cache1.hit + " Miss C1: "+ cache1.miss);
        System.out.println("Hit C2: "+ cache2.hit + " Miss C2: "+ cache2.miss);
        System.out.println("Hit C3: "+ cache3.hit + " Miss C3: "+ cache3.miss);
        System.out.println("Custo total: " + custo);
    }

    void CPU(main mainInstruction, int operation) { // recebe uma linha da matriz memoryData
        // interpreta os dados do registrador (registrador apontando os dados da RAM)
        int result;
        
        switch (operation) {
            // Soma
            case 1: {
                System.out.println("\n SOMA");
                mainInstruction.opCode = 0;
                result = mainCPU(mainInstruction, Null, Null);// fazendo a soma
                mainInstruction.opCode = 2;
                mainCPU(mainInstruction, result, 3);// salvando na memória

                printResult(mainInstruction, 1);
                break;

            }

            case 2: {
                System.out.println("\n SUBTRAÇÃO");
                mainInstruction.opCode = 1;
                result = mainCPU(mainInstruction, Null, Null);// fazendo a subtração
                mainInstruction.opCode = 2;
                mainCPU(mainInstruction, result, 3);// salvando na memória
                printResult(mainInstruction, 2);
                break;
            }

            case 3: {
                System.out.println("\nMULTIPLICACAO");
                multiply(mainInstruction);

                printResult(mainInstruction, 3);

                break;

            }
            case 4: {
                System.out.println("\nDIVISÃO :");

                divide(mainInstruction);
                printResult(mainInstruction, 4);

                break;

            }

            case 5: {
                System.out.println("\nEXPONENCIAL");
                exponential(mainInstruction);
                printResult(mainInstruction, 5);

                break;
            }

            case 6: {
                System.out.println("\nRAIZ QUADRADA");
                squareRoot(mainInstruction);
                printResult(mainInstruction, 6);

                break;

            }
            case 7: {
                System.out.println("\nCALCULAR HIPOTENUSA");
                hipotenusa(mainInstruction);
                printResult(mainInstruction, 7);

                break;
            }

            case 8: {
                System.out.println("\nAREA TRIANGULO");
                triangleArea(mainInstruction);
                printResult(mainInstruction, 8);

                break;
            }

            case 9: {
                System.out.println("\nRAIZ QUALQUER");
                root(mainInstruction);
                printResult(mainInstruction, 9);

                break;
            }
            case -1: {
                mainInstruction.opCode = -1;
                System.out.println("HALT: O programa sera encerrado");
                mainCPU(mainInstruction, Null, Null);
            }

        }
    }
    int[] returnMemAddress(main mainInstruction, int where)
    {
        int[] Return = new int[2];
        Return[0] = mainInstruction.content[where-1].end[0];
        Return[1] = mainInstruction.content[where-1].end[1];
        return Return;
    }

    int Sum(main mainInstruction) {

        // buscar na RAM
        mainInstruction.opCode = 3;
        int conteudoRam1 = mainCPU(mainInstruction, Null, 1); // 1
        int conteudoRam2 = mainCPU(mainInstruction, Null, 2); // 2
        int sum = conteudoRam1 + conteudoRam2;
        return sum;

    }
    int sub(main mainInstruction) {
        // sub
        int sub = 0;
        mainInstruction.opCode = 3;
        int conteudoRam1 = mainCPU(mainInstruction, Null, 1); // 1
        int conteudoRam2 = mainCPU(mainInstruction, Null, 2); // 2

        sub = conteudoRam1 - conteudoRam2;

        return sub;

    }

    void multiply(main mainInstruction) {

        
        int[] memAddress2 = new int[2];
        int[] memAddress3 = new int[2];
        memAddress2 = returnMemAddress(mainInstruction, 2);
        memAddress3 = returnMemAddress(mainInstruction, 3);

        mainInstruction.opCode = 3;// retornar
        int ram1Content = mainCPU(mainInstruction, Null, 1);
        int ram2Content = mainCPU(mainInstruction, Null, 2);

        int result = 0;
        if (ram1Content == 1 || ram2Content == 1) {

            if (ram1Content == 1) {

                mainInstruction.opCode = 2;
                mainCPU(mainInstruction, ram2Content, 3);

            } else {
                mainInstruction.opCode = 2;
                mainCPU(mainInstruction, ram1Content, 3);

            }

        } else if (ram1Content == 0 || ram2Content == 0) {
            mainInstruction.opCode = 2;
            mainCPU(mainInstruction, 0, 3);
        } else {

            mainInstruction.opCode = 3;
            int taken = mainCPU(mainInstruction, Null, 1);
            mainInstruction.opCode = 2;
            mainCPU(mainInstruction, taken, 3);

            mainInstruction.content[1].end = memAddress3;


            for (int i = 0; i < ram2Content - 1; i++) {

                mainInstruction.opCode = 0;
             
                result = mainCPU(mainInstruction, Null, Null);
                mainInstruction.opCode = 2;
                mainCPU(mainInstruction, result, 3);

            }

            mainInstruction.content[1].end = memAddress2;

        }

    }

    void exponential(main mainInstruction) {
        int[] memAddress2 = new int[2];
        int[] memAddress3 = new int[2];

        memAddress2 = returnMemAddress(mainInstruction, 2);
        memAddress3 = returnMemAddress(mainInstruction, 3);
        mainInstruction.opCode = 3;
        int ram1Content = mainCPU(mainInstruction, Null, 1);
        int ram2Content = mainCPU(mainInstruction, Null, 2);

        if (ram1Content == 1 || ram2Content == 1) {
            mainInstruction.opCode = 2;
            mainCPU(mainInstruction, ram1Content, 3);

        } else if (ram2Content == 0) {
            mainInstruction.opCode = 2;
            mainCPU(mainInstruction, 1, 3);
        } else {

            mainInstruction.opCode = 3;
            int taken = mainCPU(mainInstruction, Null, 1);
            mainInstruction.opCode = 2;
            mainCPU(mainInstruction, taken, 3);

            mainInstruction.content[1].end = memAddress3;
        

            for (int i = 0; i < ram2Content - 1; i++) {

                multiply(mainInstruction);
            }

            mainInstruction.content[1].end = memAddress2;

        }
    }

    void squareRoot(main mainInstruction) {

        mainInstruction.opCode = 3;
        int result;
        int ram1Content = mainCPU(mainInstruction, Null, 1);
        mainInstruction.opCode = 2;
        mainCPU(mainInstruction, ram1Content, 3);
        int cont = 0, aux = ram1Content;
        for (int i = 1; aux > 0; i += 2) {
            mainInstruction.opCode = 2;
            mainCPU(mainInstruction, i, 2);
            mainCPU(mainInstruction, aux, 1);

            mainInstruction.opCode = 1;
            result = mainCPU(mainInstruction, Null, Null);
            mainInstruction.opCode = 2;
            mainCPU(mainInstruction, result, 3);
            mainInstruction.opCode = 3;
            aux = mainCPU(mainInstruction, Null, 3);
            cont++;
        }
        mainInstruction.opCode = 2;
   
        mainCPU(mainInstruction, ram1Content, 1);
        mainCPU(mainInstruction, cont, 3);
    }

    void triangleArea(main mainInstruction) {
        mainInstruction.opCode = 3;
        int aux1 = mainCPU(mainInstruction, Null, 1);
        int aux2 = mainCPU(mainInstruction, Null, 2);
        multiply(mainInstruction);
        mainInstruction.opCode = 3;
        int result1 = mainCPU(mainInstruction, Null, 3);
        mainInstruction.opCode = 2;
        mainCPU(mainInstruction, result1, 1);
        mainCPU(mainInstruction, 2, 2);
        mainCPU(mainInstruction, 0, 3);
        divide(mainInstruction);
        mainInstruction.opCode = 3;
        int result = mainCPU(mainInstruction, Null, 3);
        mainInstruction.opCode = 2;
        mainCPU(mainInstruction, result, 3);
        mainCPU(mainInstruction, aux1, 1);
        mainCPU(mainInstruction, aux2, 2);

    }

    void divide(main mainInstruction) {
        int result = 1, cont = 0;

        int[] memAddress1 = new int[2];
        int[] memAddress2 = new int[2];

        memAddress1 = returnMemAddress(mainInstruction, 1);
        memAddress2 = returnMemAddress(mainInstruction, 2);


        mainInstruction.opCode= 3;// trazer
        int ram1Content = mainCPU(mainInstruction, Null, 1);
        int RAM1CLONE = ram1Content;
        int ram2Content = mainCPU(mainInstruction, Null, 2);

        if (ram1Content < ram2Content) {
            mainInstruction.opCode = 2;
            mainCPU(mainInstruction, 0, 3);
            mainCPU(mainInstruction, ram1Content, 4);
        } else if (ram2Content == 0) {
            mainInstruction.opCode = 2;
            mainCPU(mainInstruction, -1, 3);
            mainCPU(mainInstruction, -1, 4);
        } else {
            while (ram1Content >= ram2Content) {

                mainInstruction.opCode = 1;
                result = mainCPU(mainInstruction, Null, Null);

                mainInstruction.opCode = 2;
                mainCPU(mainInstruction, result, 3);// salvando

                mainInstruction.content[0].end = mainInstruction.content[2].end;

                mainInstruction.opCode = 3;
         
                ram1Content = mainCPU(mainInstruction, Null, 1);

                cont++;
            }
            mainInstruction.content[0].end =  memAddress1;
            mainInstruction.content[1].end = memAddress2;// reestaura os endereços de memoria

            mainInstruction.opCode = 2;// salva os conteudos nos lugares que a gente quer usar para fazer a mult
            mainCPU(mainInstruction, cont, 1);
            mainCPU(mainInstruction, ram2Content, 2);
            multiply(mainInstruction);

            mainCPU(mainInstruction, RAM1CLONE, 1);// salva a ram1 no lugar que quer utilizar p fazer subtração
            mainInstruction.opCode = 3;
            int aux = mainCPU(mainInstruction, Null, 3);// coloca o resultado da mult pra participar da sub

            mainInstruction.opCode = 2;// sub
            mainCPU(mainInstruction, aux, 2);

            mainInstruction.opCode = 1;
            int rest = mainCPU(mainInstruction, Null, Null);

            mainInstruction.opCode = 2;// salva

            mainCPU(mainInstruction, RAM1CLONE, 1);// reestaura conteudos
            mainCPU(mainInstruction, ram2Content, 2);

            mainCPU(mainInstruction, cont, 3);

            mainCPU(mainInstruction, rest, 4);

        }

    }


    void hipotenusa(main mainInstruction) {

        int resultado = 0, resultado2 = 0, aux;
        main instruction = new main ();
        instruction = mainInstruction;

        instruction.opCode = 3;
        int n1 = mainCPU(mainInstruction, Null, 1);
        aux = mainCPU(mainInstruction, Null, 2);
        instruction.opCode = 2;
      
        mainCPU(mainInstruction, 2, 2);

        ////////////// inicio da exponenciação de a e b/////////////////
        exponential(instruction);
        instruction.opCode = 3;
        resultado = mainCPU(mainInstruction, Null, 3);
        instruction.opCode = 2;
        mainCPU(mainInstruction, resultado, 3);
        mainCPU(mainInstruction, aux, 1);
        exponential(instruction);
        instruction.opCode = 3;
        resultado2 = mainCPU(mainInstruction, Null, 3);

        /////////////////////////// altera os valores na memoria principal///////////
        instruction.opCode = 2;
        
        mainCPU(mainInstruction, resultado, 1);
        mainCPU(mainInstruction, resultado2, 2);
        resultado = 0;
        ////////////// inicio da nova instrução : soma ///////////////////////////
        resultado = Sum(instruction);
        instruction.opCode = 2;
        mainCPU(mainInstruction, resultado, 1);

        ///////// guarda na memoria principal o resultado e inicia a ultima
        ///////// instrução//////////
        squareRoot(instruction);
        mainCPU(mainInstruction, n1, 1);
        mainCPU(mainInstruction, aux, 2);

    }

    void root(main mainInstruction) {
        mainInstruction.opCode = 3;
        int ram1Content = mainCPU(mainInstruction, Null, 1);
        int ram2Content = mainCPU(mainInstruction, Null, 2);
        int rootToBeTaken = ram1Content;
        int kindOfRoot = ram2Content;

        int result;

        if (kindOfRoot == 1 || kindOfRoot == 0) {
            mainInstruction.opCode = 2;
            mainCPU(mainInstruction, -9876, 3);
        } else if (kindOfRoot % 2 == 0 && rootToBeTaken < 0) {
            mainInstruction.opCode = 2;
            mainCPU(mainInstruction, -9999, 3);
        } else if (kindOfRoot % 2 != 0 && rootToBeTaken < 0) {
            rootToBeTaken *= -1;
            for (int i = 0; i <= rootToBeTaken; i++) {
                mainInstruction.opCode = 2;
                mainCPU(mainInstruction, i, 1);
                mainCPU(mainInstruction, kindOfRoot, 2);
                exponential(mainInstruction);
                mainInstruction.opCode = 3;
                result = mainCPU(mainInstruction, Null, 3);

                if (result > rootToBeTaken) // se ele for maior que a raíz, ele não é a raíz e nenhum outro i vai ser //
                                            // pois a partir daqui os resultados só vão ultrapassar o valor do numero
                {
                    mainInstruction.opCode = 2;
                    mainCPU(mainInstruction, -6789, 3);
                    mainCPU(mainInstruction, ram1Content, 1);
                    mainCPU(mainInstruction, ram2Content, 2);
                    break;
                } else if (result == rootToBeTaken) {// se o resultado for igual ao numero que vc quer tirar raíz, o i é
                                                     // a raiz desse numero
                    mainInstruction.opCode = 2;
                    mainCPU(mainInstruction, -i, 3);
                    mainCPU(mainInstruction, ram1Content, 1);
                    mainCPU(mainInstruction, ram2Content, 2);
                    break;
                }

            }

        } else {
            for (int i = 0; i <= rootToBeTaken; i++) {
                mainInstruction.opCode = 2;
                mainCPU(mainInstruction, i, 1);
                mainCPU(mainInstruction, kindOfRoot, 2);
                exponential(mainInstruction);
                mainInstruction.opCode = 3;
                result = mainCPU(mainInstruction, Null, 3);

                if (result > rootToBeTaken) // se ele for maior que a raíz, ele não é a raíz e nenhum outro i vai ser //
                                            // pois a partir daqui os resultados só vão ultrapassar o valor do numero
                {
                    
                    mainInstruction.opCode = 2;
                    mainCPU(mainInstruction, -6789, 3);
                    mainCPU(mainInstruction, ram1Content, 1);
                    mainCPU(mainInstruction, ram2Content, 2);
                    break;
                } else if (result == rootToBeTaken) {// se o resultado for igual ao numero que vc quer tirar raíz, o i é
                                                     // a raiz desse numero
                    
                    mainInstruction.opCode = 2;
                    mainCPU(mainInstruction, i, 3);
                    mainCPU(mainInstruction, ram1Content, 1);
                    mainCPU(mainInstruction, ram2Content, 2);
                    break;
                }

            }
        }

    }

    //FUNÇÃO DEPOIS DO HALT
    void saveAfterHalt (Cache1 cache1, Cache2 cache2, Cache3 cache3, bloco[]memoryData)
    {
        for(int i = 0; i < SIZECACHE1; i++)
        {
            if(cache1.bloco[i].atualizado == true)
            {
                cache1.bloco[i].hit = 0;
                cache1.bloco[i].atualizado = false;
                memoryData[cache1.bloco[i].tag] = cache1.bloco[i];
            }
        }
        for(int i = 0; i < SIZECACHE2; i++)
        {
            if(cache2.bloco[i].atualizado == true)
            {
                cache2.bloco[i].hit = 0;
                cache2.bloco[i].atualizado = false;
                memoryData[cache2.bloco[i].tag] = cache2.bloco[i];
            }
        }

        for(int i = 0; i < SIZECACHE3; i++)
        {
            if(cache3.bloco[i].atualizado == true)
            {
                cache3.bloco[i].hit = 0;
                cache3.bloco[i].atualizado = false;
                memoryData[cache3.bloco[i].tag] = cache3.bloco[i];
            }
        }
    }


    //FUNÇÕES DE GUARDAR
    //cache1 cheia
    void toCache1FullS(int palavraID, int toBeStored, int popFromWhere, bloco[] memoryData, Cache1 cache1, Cache2 cache2, Cache3 cache3)
    {
        int idle = 0;
        for (int i = 1; i < SIZECACHE1; i++)
        {
            if (cache1.bloco[i].T < cache1.bloco[idle].T)
            {
                idle = i;
            }
            else if (cache1.bloco[i].T == cache1.bloco[idle].T)
            {
                if (cache1.bloco[i].hit < cache1.bloco[idle].hit)
                {
                    idle = i;
                }else if (cache1.bloco[i].hit == cache1.bloco[idle].hit)
                {
                    if(i < idle)
                    {
                        idle = i;
                    }
                }
            }

        }
        bloco from1to2 = new bloco();
        bloco from2to1 = new bloco();

        from1to2 = popFromCache1(cache1, idle);
        from2to1 = popFromCache2(cache2, popFromWhere);

        insertIntoCache1(cache1, from2to1);
        insertIntoCache2(cache2, from1to2);
        cache1.bloco[cache1.counter-1].T = time;

        for (int i = 0; i < 4; i++)
        {
            if (cache1.bloco[cache1.counter-1].Palavra[i].endereco == palavraID)
            {
                cache1.bloco[cache1.counter-1].Palavra[i].valor = toBeStored;
                cache1.bloco[cache1.counter-1].hit++;
                break;
            }
        }
        

        if(cache1.bloco[cache1.counter-1].atualizado == false)
        {
            cache1.bloco[cache1.counter-1].atualizado = true;
        }


    }

    //chache1 com espaço
    void toCache1NotFullS(int palavraID, int toBeStored, int popFromWhere, bloco[] memoryData, Cache1 cache1, Cache2 cache2, Cache3 cache3)
    {
        bloco aux = new bloco();

        aux = popFromCache2(cache2, popFromWhere);
        
        insertIntoCache1(cache1, aux);
      
        cache1.bloco[cache1.counter-1].T = time;
        for (int i = 0; i < 4; i++)
        {
            if (cache1.bloco[cache1.counter-1].Palavra[i].endereco == palavraID)
            {
                cache1.bloco[cache1.counter-1].Palavra[i].valor = toBeStored;
                break;
            }
        }
      
        cache1.bloco[cache1.counter-1].hit++;

        if(cache1.bloco[cache1.counter-1].atualizado == false)
        {
            cache1.bloco[cache1.counter-1].atualizado = true;
        }

    }
    //FUNÇÕES QUE APENAS MOVEM DE UMA RAM PRA OUTRA
    //cache2 cheia
    void toCache2Full(int popFromWhere, bloco[] memoryData, Cache1 cache1, Cache2 cache2, Cache3 cache3)
    {
        int idle = 0;
        for (int i = 1; i < SIZECACHE2; i++)
        {
            if (cache2.bloco[i].T < cache2.bloco[idle].T)
            {
                idle = i;
            }
            else if (cache2.bloco[i].T == cache2.bloco[idle].T)
            {
                if (cache2.bloco[i].hit < cache2.bloco[idle].hit)
                {
                    idle = i;
                }else if (cache2.bloco[i].hit == cache2.bloco[idle].hit)
                {
                    if(i < idle)
                    {
                        idle = i;
                    }
                }
            }
        }
        bloco from3to2 = new bloco();
        bloco from2to3 = new bloco();
     

        from3to2 = popFromCache3(cache3, popFromWhere);

        from2to3 = popFromCache2(cache2, idle);

        insertIntoCache2(cache2, from3to2);
        insertIntoCache3(cache3, from2to3);
    }
    //chache2 com espaço
    void toCache2NotFull(int popFromWhere, bloco[] memoryData, Cache1 cache1, Cache2 cache2, Cache3 cache3)
    {
        bloco aux = new bloco();
        aux = popFromCache3(cache3, popFromWhere);
        insertIntoCache2(cache2, aux);

    }

    //cache3 cheia
    void toCache3Full(Cache3 cache3, bloco[] memoryData, int blocoID)
    {
        bloco aux = new bloco();

        int idle = 0;
        for (int i = 1; i < SIZECACHE3; i++)
        {
            if (cache3.bloco[i].T < cache3.bloco[idle].T)
            {
                idle = i;
            }
            else if (cache3.bloco[i].T == cache3.bloco[idle].T)
            {
                if (cache3.bloco[i].hit < cache3.bloco[idle].hit)
                {
                    idle = i;
                }else if (cache3.bloco[i].hit == cache3.bloco[idle].hit)
                {
                    if(i < idle)
                    {
                        idle = i;
                    }
                }
            }
        }

        if(cache3.bloco[idle].atualizado == false)
        {
            aux = popFromCache3(cache3, idle);
        }else
        {
            cache3.bloco[idle].hit = 0;
            cache3.bloco[idle].atualizado = false;
            memoryData[cache3.bloco[idle].tag] = cache3.bloco[idle];
            popFromCache3(cache3, idle);
        }

        insertIntoCache3(cache3, memoryData[blocoID]);
       
 
    }

    //chache3 com espaço
    void toCache3NotFull(Cache3 cache3, bloco[] memoryData, int blocoID)
    {
        insertIntoCache3(cache3, memoryData[blocoID]);

    }


    //FUNÇÕES DE TRAZER
    //cache1 cheia
    int toCache1FullT(int palavraID, int popFromWhere, bloco[] memoryData, Cache1 cache1, Cache2 cache2, Cache3 cache3)
    {
   
        int idle = 0;
        for (int i = 1; i < SIZECACHE1; i++)
        {
            if (cache1.bloco[i].T < cache1.bloco[idle].T)
            {
                idle = i;
            }
            else if (cache1.bloco[i].T == cache1.bloco[idle].T)
            {
                if (cache1.bloco[i].hit < cache1.bloco[idle].hit)
                {
                    idle = i;
                }else if (cache1.bloco[i].hit == cache1.bloco[idle].hit)
                {
                    if(i < idle)
                    {
                        idle = i;
                    }
                }
            }
        }
        bloco from1to2 = new bloco();
        bloco from2to1 = new bloco();

        from1to2 = popFromCache1(cache1, idle);
        from2to1 = popFromCache2(cache2, popFromWhere);

        insertIntoCache1(cache1, from2to1);
        insertIntoCache2(cache2, from1to2);
        cache1.bloco[cache1.counter-1].T = time;
        

        for (int i = 0; i < 4; i++)
        {
            if (cache1.bloco[cache1.counter-1].Palavra[i].endereco == palavraID)
            {
                cache1.bloco[cache1.counter-1].hit++;
                return cache1.bloco[cache1.counter-1].Palavra[i].valor;
            }
        }
        

        return -4;

    }

    int toCache1NotFullT(int palavraID, int popFromWhere, bloco[] memoryData, Cache1 cache1, Cache2 cache2, Cache3 cache3)
    {
        bloco aux = new bloco();

        aux = popFromCache2(cache2, popFromWhere);
        insertIntoCache1(cache1, aux);
        cache1.bloco[cache1.counter-1].T = time;
        
        for (int i = 0; i < 4; i++)
        {   
            if (cache1.bloco[cache1.counter-1].Palavra[i].endereco == palavraID)
            {
                
                cache1.bloco[cache1.counter-1].hit++;
                return cache1.bloco[cache1.counter-1].Palavra[i].valor;
            }
        }
        return -3;
        
    }
   
    void storeInMem(int opcode, int blocoID, int palavraID, int toBeStored, bloco[] memoryData, Cache1 cache1, Cache2 cache2, Cache3 cache3) 
    {
        time++;
        int flag = 0;
        int index = 0;
        // flag 1 = encontrado na cache1, 2 = encontrado na cache2, 3 = encontrado na cache3, 4 = encontrado na ram
      
        for (int i = 0; i < SIZECACHE1; i++)
        {
            if(cache1.bloco[i].tag == blocoID)
            {
                flag = 1;
                index = i;
                break;
            }
        }
        if (flag == 0)
        {
            for (int i = 0; i < SIZECACHE2; i++)
            {
                if(cache2.bloco[i].tag == blocoID)
                {
                    flag = 2;
                    index = i;
                    break;
                }
            }
        }
        if (flag == 0)
        {
            for (int i = 0; i < SIZECACHE3; i++)
            {
                if(cache3.bloco[i].tag == blocoID)
                {
                    flag = 3;
                    index = i;
                    break;
                }
            }
        }
        if (flag == 0)
        {
            flag = 4;
            index = blocoID;
           
        }        

        if (flag == 1)
        {
            cache1.hit++;
        }
        if (flag == 2)
        {
            cache2.hit++;
            cache1.miss++;
        } else if (flag == 3)
        {
            cache3.hit++;
            cache1.miss++;
            cache2.miss++;
        } else if (flag == 4)
        {
            cache1.miss++;
            cache2.miss++;
            cache3.miss++;
        }

        if(flag == 1)//se ja estiver na cache1
        {
            custo += C1;
            for (int i = 0; i < 4; i++)
            {
                if (cache1.bloco[index].Palavra[i].endereco == palavraID)
                {
                    cache1.bloco[index].Palavra[i].valor = toBeStored;
                    break;
                }
            }
            cache1.bloco[index].hit++;
            if (cache1.bloco[index].atualizado == false)
            {
                cache1.bloco[index].atualizado = true;
            }

        }else if (flag == 2)//se estiver na cache2
        {  
            custo += C2;
            if(cache1Status(cache1) != 1)// se o 1 nao estiver cheio eu posso trazer o bloco pra ele
            {
                toCache1NotFullS(palavraID, toBeStored, index, memoryData, cache1, cache2, cache3);
            }else//se tiver cheio tenho que achar qual retirar dele
            {
                toCache1FullS(palavraID, toBeStored, index, memoryData, cache1, cache2, cache3);
            }
        }else if (flag == 3)//se estiver na cache3
        {
            custo += C3;
            if(cache2Status(cache2) != 1)// se a 2 nao estiver cheia eu posso trazer o bloco pra ele
            {
                toCache2NotFull(index, memoryData, cache1, cache2, cache3);

                if (cache1Status(cache1) != 1)// se a cache 1 n estiver cheia
                {
                    toCache1NotFullS(palavraID, toBeStored, cache2.counter-1, memoryData, cache1, cache2, cache3);

                }else//1 estando cheia
                {
                    toCache1FullS(palavraID, toBeStored, cache2.counter-1, memoryData, cache1, cache2, cache3);

                }
            }else // cache 2 está cheia
            {
                toCache2Full(index, memoryData, cache1, cache2, cache3);
                
                toCache1FullS(palavraID, toBeStored, cache2.counter-1, memoryData, cache1, cache2, cache3);


            }
        }else if (flag == 4)
        {
            custo += CRAM;
            if(cache3Status(cache3) != 1)
            {
               
                toCache3NotFull(cache3, memoryData, blocoID);

                if(cache2Status(cache2) != 1)//se a 2 n estiver cheia
                {  
                    toCache2NotFull(cache3.counter-1, memoryData, cache1, cache2, cache3);
                    
                    if (cache1Status(cache1) != 1)// se a 1 n estiver cheia
                    {   
                        toCache1NotFullS(palavraID, toBeStored, cache2.counter-1, memoryData, cache1, cache2, cache3);

                    }else // se a cache 1 estiver cheia
                    {   
                        toCache1FullS(palavraID, toBeStored, cache2.counter-1, memoryData, cache1, cache2, cache3);
                    }
                }else
                {
                    toCache2Full(cache3.counter-1, memoryData, cache1, cache2, cache3);
                    toCache1FullS(palavraID, toBeStored, cache2.counter-1, memoryData, cache1, cache2, cache3);
                }

            }else //se a cache 3 estiver cheia
            {
              
                toCache3Full(cache3, memoryData, blocoID);
                toCache2Full(cache3.counter-1, memoryData, cache1, cache2, cache3);
                toCache1FullS(palavraID, toBeStored, cache2.counter-1, memoryData, cache1, cache2, cache3);
                    
                

            }
        }
    }

    int takeFromMem(int opcode, int blocoID, int palavraID, bloco[] memoryData, Cache1 cache1, Cache2 cache2, Cache3 cache3) 
    {
        int flag = 0;
        int index = 0;
        time++;
        
        // flag 1 = encontrado na cache1, 2 = encontrado na cache2, 3 = encontrado na cache3, 4 = encontrado na ram
        for (int i = 0; i < SIZECACHE1; i++)
        {
            if(cache1.bloco[i].tag == blocoID)
            {
                flag = 1;
                index = i;
                break;
            }
        }
        if (flag == 0)
        {
            
            for (int i = 0; i < SIZECACHE2; i++)
            {
                if(cache2.bloco[i].tag == blocoID)
                {
                    flag = 2;
                    index = i;

                    break;
                }
            }
        }
        if (flag == 0)
        {   
   
            for (int i = 0; i < SIZECACHE3; i++)
            {
                if(cache3.bloco[i].tag == blocoID)
                {
                    flag = 3;
                    index = i;

                    break;
                }
            }
        }
        if (flag == 0)
        {
            flag = 4;//store
            index = blocoID;

        }        
        if (flag == 1)
        {
            cache1.hit++;
        }
        if (flag == 2)
        {
            cache2.hit++;
            cache1.miss++;
        } else if (flag == 3)
        {
            cache1.miss++;
            cache2.miss++;
            cache3.hit++;
        } else if (flag == 4)
        {
            cache1.miss++;
            cache2.miss++;
            cache3.miss++;
        }
        
        if(flag == 1)//se ja estiver na cache1
        {
            custo += C1;
            for (int i = 0; i < 4; i++)
            {
                if (cache1.bloco[index].Palavra[i].endereco == palavraID)
                {
                    cache1.bloco[index].hit++;
                    //System.out.println("valor: " +cache1.bloco[index].Palavra[i].valor );
                    return cache1.bloco[index].Palavra[i].valor;
                }
            }
            

        }else if (flag == 2)//se estiver na cache2
        {
            custo += C2;
         
            if(cache1Status(cache1) != 1)// se o 1 nao estiver cheio eu posso trazer o bloco pra ele
            {
                return toCache1NotFullT(palavraID, index, memoryData, cache1, cache2, cache3);
            }else//se tiver cheio tenho que achar qual retirar dele
            {
                return toCache1FullT(palavraID, index, memoryData, cache1, cache2, cache3);
            }
        }else if (flag == 3)//se estiver na cache3
        {
            custo += C3;
            if(cache2Status(cache2) != 1)// se a 2 nao estiver cheia eu posso trazer o bloco pra ele
            {
                toCache2NotFull(index, memoryData, cache1, cache2, cache3);

                if (cache1Status(cache1) != 1)// se a cache 1 n estiver cheia
                {
                    return toCache1NotFullT(palavraID, cache2.counter-1, memoryData, cache1, cache2, cache3);

                }else//1 estando cheia
                {
                    return toCache1FullT(palavraID, cache2.counter-1, memoryData, cache1, cache2, cache3);

                }
            }else // cache 2 está cheia
            {
                toCache2Full(index, memoryData, cache1, cache2, cache3);
                return toCache1FullT(palavraID, cache2.counter-1, memoryData, cache1, cache2, cache3);


            }
        }else if (flag == 4)
        {
            custo += CRAM;
            if(cache3Status(cache3) != 1)//se a cache 3 n estiver cheia
            {
                toCache3NotFull(cache3, memoryData, blocoID);
                if(cache2Status(cache2) != 1)//se a 2 n estiver cheia
                {
                    toCache2NotFull(cache3.counter-1, memoryData, cache1, cache2, cache3);

                    if (cache1Status(cache1) != 1)// se a 1 n estiver cheia
                    {
                        return toCache1NotFullT(palavraID, cache2.counter-1, memoryData, cache1, cache2, cache3);

                    }else // se a cache 1 estiver cheia
                    {
                        return toCache1FullT(palavraID, cache2.counter-1, memoryData, cache1, cache2, cache3);
                    }
                }else
                {
                    toCache2Full(cache3.counter-1, memoryData, cache1, cache2, cache3);
                    toCache1FullT(palavraID, cache2.counter-1, memoryData, cache1, cache2, cache3);
                }

            }else //se a cache 3 estiver cheia
            {
                toCache3Full(cache3, memoryData, blocoID);
                toCache2Full(cache3.counter-1, memoryData, cache1, cache2, cache3);
                return toCache1FullT(palavraID, cache2.counter-1, memoryData, cache1, cache2, cache3);
                    
            }
        }

        return -99;
    }
}